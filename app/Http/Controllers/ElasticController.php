<?php

namespace App\Http\Controllers;

use Elasticsearch\ClientBuilder;

class ElasticController extends Controller
{
    public function testElastic()
    {
        $client = ClientBuilder::create()->build();

        $params = [
            'index'  => 'test',
            'type'   => 'test',
            'id'     => '1',
            'client' => [
                'verbose' => true,
                'ignore'  => 404,
            ],
        ];

        $indexParams = [
            'index' => 'test',
            'type'  => 'my_type',
            'id'    => 'my_id',
            'body'  => ['testField' => 'abc'],
        ];
        $indexing = $client->index($indexParams);
        $response = $client->get($params);
        print_r($response);
    }
}
